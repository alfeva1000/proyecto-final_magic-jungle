using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidasExtra : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            bool vidaRecuperada = GameManager.Instance.RecuperarVida();
            Destroy(this.gameObject);
        }
    }
}

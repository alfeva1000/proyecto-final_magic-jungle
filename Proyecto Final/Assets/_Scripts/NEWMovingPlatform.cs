using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWMovingPlatform : MonoBehaviour
{
	[SerializeField] public Transform[] puntosMovimientos;
	[SerializeField] public float velocidadMovimiento;
	private int siguientePlataforma = 1;
	private bool ordenPlataformas = true;

	public NEWControlJugador playerPlatformReference;
	public bool isPlayerOnPlatform;
	Vector2 directionOfPlatfom;


    private void Start()
    {
        playerPlatformReference = FindObjectOfType<NEWControlJugador>();
    }

    private void Update()
	{
		if (ordenPlataformas && siguientePlataforma + 1 >= puntosMovimientos.Length)
		{
			ordenPlataformas = false;
		}

		if (!ordenPlataformas && siguientePlataforma <= 0)
		{
			ordenPlataformas = true;
		}

		if (Vector2.Distance(transform.position, puntosMovimientos[siguientePlataforma].position) < 0.1f)
		{
			if (ordenPlataformas)
			{
				siguientePlataforma += 1;
			}
			else
			{
				siguientePlataforma -= 1;
			}
		}
		  directionOfPlatfom = (puntosMovimientos[siguientePlataforma].position - transform.position).normalized;

		//.position = directionOfPlatfom * velocidadMovimiento * Time.deltaTime;
	}
    private void FixedUpdate()
    {
        Vector3 movement = (Vector3)directionOfPlatfom * velocidadMovimiento * Time.fixedDeltaTime;
		transform.position += movement;


        if (isPlayerOnPlatform)
		{
			playerPlatformReference.transform.position += movement;

		}
	}

    private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			isPlayerOnPlatform = true;

         // other.transform.SetParent(this.transform);
		}
	}

	private void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
            isPlayerOnPlatform = false;
            playerPlatformReference.platformSpeed = Vector2.zero;
           // playerOnPlatform = null;
		}
	}
}
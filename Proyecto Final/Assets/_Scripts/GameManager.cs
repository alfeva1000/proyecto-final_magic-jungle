using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }
    public HUD hud;
    public int PuntosTotales { get { return puntosTotales; } }
    public int puntosTotales;
    public int orbPickup;
    public FinalBossController finalBossController;
    public int parch;
    

    private int vidas = 4;

    void Awake() 
    {
        finalBossController = FindObjectOfType<FinalBossController>();
        if (PlayerPrefs.HasKey("Puntos"))
        {
            puntosTotales = PlayerPrefs.GetInt("Puntos");
        }
        if (Instance == null)
        {
            Instance = this;
        } else
        {
            Debug.Log("Tienes mas de un GameManager en esta escena");
        }
        if (PlayerPrefs.HasKey("Parch"))
        {
            parch = PlayerPrefs.GetInt("Parch");
        }
    }

    public void SaveCoins()
    {
        PlayerPrefs.SetInt("Puntos", puntosTotales);
        PlayerPrefs.Save();
    }
    public void SaveParch()
    {
        PlayerPrefs.SetInt("Parch", parch);
        PlayerPrefs.Save();
    }
    public void HalveCoins()
    {
        puntosTotales = puntosTotales / 2;
        SaveCoins();
    }

    public void SumarPuntos(int puntosASumar)
    {
        puntosTotales += puntosASumar;
        hud.ActualizarPuntos(PuntosTotales);
    }
    public void Sumarparch()
    {
        parch += 1;
        
    }

    public void PerderVida()
    {
        vidas -= 1;

        if(vidas == 0)
        {
            HalveCoins();

            Physics2D.IgnoreLayerCollision(6, 3, false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
            
        }
        hud.DesactivarVida(vidas);
    }

    public bool RecuperarVida()
    {
        if (vidas == 4)
        {
            return false;
        }

        hud.ActivarVida(vidas);
        vidas += 1;
        return true;
    }

    public void MuerteAbismo()
    {
        vidas-= 4;

        if(vidas <= 0)
        {
            HalveCoins();
            //Se reinicia el nivel.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
        }
        //hud.DesactivarVida(vidas);
    }
    void Update()
    {
        if (orbPickup == 2)
        {
            orbPickup = 0;
            
            finalBossController.Dmg();
        }
    }
}

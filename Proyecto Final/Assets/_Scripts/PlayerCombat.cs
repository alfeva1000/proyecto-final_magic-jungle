using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    [SerializeField] private float vida;
    private TarodevController.PlayerController playerController;
    [SerializeField] private float tiempoPerdidaControl;

    private Animator animator;

    private void Start()
    {
        playerController = GetComponent<TarodevController.PlayerController>();
        animator = GetComponent<Animator>();
    }


    public void TomarDaño(float daño)
    {
        vida-= daño;
    }

    public void TomarDaño(float daño, Vector2 posicion)
    {
        vida-= daño;
        animator.SetTrigger("Player Damaged");
        StartCoroutine(PerderControl());
        playerController.Rebote(posicion);
    }

    private IEnumerator PerderControl()
    {
        playerController.CanMove = false;
        yield return new WaitForSeconds(tiempoPerdidaControl);
        playerController.CanMove = true;
    }
}

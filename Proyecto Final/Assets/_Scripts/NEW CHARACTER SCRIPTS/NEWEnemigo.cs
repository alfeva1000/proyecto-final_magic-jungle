using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWEnemigo : MonoBehaviour
{
    [SerializeField] private GameObject efecto;
    private Animator animator;
    public Vector3 teleportPosition;
   // public string enemigos = "Enemies"


    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    private void Start()
    {
       // GameObject[] enemies = GameObject.FindObjectWithTag(enemigos);
       // foreach (GameObject enemies)
    }
    //Primera parte que va con las vidas del personaje
    private void OnCollisionEnter2D(Collision2D other)
   {
        if(other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<NEWPlayerCombat>().TomarDaño(1, other.GetContact(0).normal);
            GameManager.Instance.PerderVida();
        }
   }

    //Segunda parte que va con las vidas de los enemigos

    [SerializeField] private float vidaEnemigo;

    public void TomarDañoEnemigo(float dañoEnemigo)
    {
        animator.SetTrigger("Golpe");
        vidaEnemigo -= dañoEnemigo;

        if (vidaEnemigo <= 0)
        {
            Muerte();
        }
    }

    private void Muerte()
    {
        Instantiate(efecto, transform.position, transform.rotation);
        transform.position = teleportPosition;
        Debug.Log("Teleported to: " + teleportPosition);
    } 

}

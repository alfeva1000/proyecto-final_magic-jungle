using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWCambiarColor : MonoBehaviour
{
    //Publico
    public GameObject enemigoLigth;
    public GameObject enemigoDark;
    public GameObject enemigoLigth2;
    public GameObject enemigoDark2;
    private CircleCollider2D[] colliderconderechos;
    private CircleCollider2D[] collidersinderechos;
    private BoxCollider2D[] bcolliderconderechos;
    private BoxCollider2D[] bcollidersinderechos;
    //Privado
    private SpriteRenderer skin;
    //private Color colors;
    public bool Change = false;
    // Start is called before the first frame update
    void Start()
    {
        //colors = new Color
        skin = GetComponent<SpriteRenderer>();
        colliderconderechos = enemigoLigth.GetComponentsInChildren<CircleCollider2D>();
        collidersinderechos = enemigoDark.GetComponentsInChildren<CircleCollider2D>();
        bcolliderconderechos = enemigoLigth.GetComponentsInChildren<BoxCollider2D>();
        bcollidersinderechos = enemigoDark.GetComponentsInChildren<BoxCollider2D>();

        // Get colliders from second set of GameObjects and append them to the arrays
        CircleCollider2D[] colliderconderechos2 = enemigoLigth2.GetComponentsInChildren<CircleCollider2D>();
        CircleCollider2D[] collidersinderechos2 = enemigoDark2.GetComponentsInChildren<CircleCollider2D>();
        BoxCollider2D[] bcolliderconderechos2 = enemigoLigth2.GetComponentsInChildren<BoxCollider2D>();
        BoxCollider2D[] bcollidersinderechos2 = enemigoDark2.GetComponentsInChildren<BoxCollider2D>();

        // Concatenate the arrays
        colliderconderechos = ConcatArrays(colliderconderechos, colliderconderechos2);
        collidersinderechos = ConcatArrays(collidersinderechos, collidersinderechos2);
        bcolliderconderechos = ConcatArrays(bcolliderconderechos, bcolliderconderechos2);
        bcollidersinderechos = ConcatArrays(bcollidersinderechos, bcollidersinderechos2);

        foreach (var items in collidersinderechos)
        {
            items.enabled = true;
        }

        foreach (var items in bcollidersinderechos)
        {
            items.enabled = false;
        }

        foreach (var items in colliderconderechos)
        {
            items.enabled = false;
        }

        foreach (var items in bcolliderconderechos)
        {
            items.enabled = true;
        }
    }
    T[] ConcatArrays<T>(T[] array1, T[] array2)
    {
        T[] newArray = new T[array1.Length + array2.Length];
        array1.CopyTo(newArray, 0);
        array2.CopyTo(newArray, array1.Length);
        return newArray;
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire4") && Change == false)
        {
            Change = true;
            skin.color = new Color(0.545f,0.0f, 1.0f);
            foreach (var items in collidersinderechos)
            {
                items.enabled = false;
            }

            foreach (var items in bcollidersinderechos)
            {
                items.enabled = true;
            }

            foreach (var items in colliderconderechos)
            {
                items.enabled = true;
            }

            foreach (var items in bcolliderconderechos)
            {
                items.enabled = false;
            }
        }

        else if(Input.GetButtonDown("Fire4") && Change == true)
        {
            Change = false;
            skin.color = Color.white;
            foreach (var items in collidersinderechos)
            {
                items.enabled = true;
            }

            foreach (var items in bcollidersinderechos)
            {
                items.enabled = false;
            }

            foreach (var items in colliderconderechos)
            {
                items.enabled = false;
            }

            foreach (var items in bcolliderconderechos)
            {
                items.enabled = true;
            }
        }
    }


}

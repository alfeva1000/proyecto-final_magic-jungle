using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColisionFinal4 : MonoBehaviour
{
    public GameManager gameManager;

    
    private void OnCollisionEnter2D(Collision2D other)
    {
        gameManager = FindObjectOfType<GameManager>();
        if (other.gameObject.CompareTag("Player"))
        {
            gameManager.SaveCoins();
            gameManager.SaveParch();
            SceneManager.LoadScene("LobbyFinal");
        }
    }
}

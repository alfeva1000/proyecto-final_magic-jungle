using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWControlJugador : MonoBehaviour
{
    
    private Rigidbody2D rb2D;

    [Header("Movimiento")]
    private float movimientoHorizontal = 0f;
    [SerializeField] private float velocidadDeMovimiento;
    [Range(0, 0.3f)][SerializeField] private float suavizadodeMovimiento;
    private Vector3 velocidad = Vector3.zero;
    public Vector2 platformSpeed=Vector2.zero;
    private bool mirandoDerecha = true;
    public bool CanMove = true;
    [SerializeField] private Vector2 velocidadRebote;
    public bool lookCheck;
    public GameManager gameManager; 

    [Header("Salto")]
    [SerializeField] private float fuerzaDeSalto;
    [SerializeField] private LayerMask queEsSuelo;
    [SerializeField] private Transform controladorSuelo;
    [SerializeField] private Vector3 dimensionesCaja;
    [SerializeField] private bool enSuelo;
    private bool salto = false;

    [Header("Dash")]

    [SerializeField] private float velocidadDash;
    [SerializeField] private float tiempoDash;
    [SerializeField] private TrailRenderer trailRenderer;
    private float gravedadInicial;
    private bool PuedeHacerDash = true;
    private float CoolDownDash = 1f;
    public float velocidadDashOriginal = 20f;
    public float coolDownDashOriginal = 1f;
    public float velocidadDeMovimientoOriginal = 250f;


    [Header("Animacion")]
    private Animator animator;

    private bool dashSpeedUpgraded = false;
    private bool movementSpeedUpgraded = false;
    private bool dashCoolDownUpgraded = false;
    private void Start()
    {
        dashSpeedUpgraded = PlayerPrefs.GetInt("dashSpeedUpgraded", 0) == 1;
        movementSpeedUpgraded = PlayerPrefs.GetInt("movementSpeedUpgraded", 0) == 1;
        dashCoolDownUpgraded = PlayerPrefs.GetInt("dashCoolDownUpgraded", 0) == 1;
        velocidadDash = PlayerPrefs.GetFloat("velocidadDash", 20f);
        velocidadDeMovimiento = PlayerPrefs.GetFloat("velocidadDeMovimiento", 250f);
        CoolDownDash = PlayerPrefs.GetFloat("CoolDownDash", 1f);

        

        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        gravedadInicial = rb2D.gravityScale;
    }

    private void Update()
    {
        movimientoHorizontal = Input.GetAxisRaw("Horizontal") * velocidadDeMovimiento;

        animator.SetFloat("Horizontal", Mathf.Abs(movimientoHorizontal));
        animator.SetFloat("VelocidadY", rb2D.velocity.y);

        if(Input.GetButtonDown("Jump"))
        {
            salto = true;
        }

        if(Input.GetKeyDown(KeyCode.LeftShift) && PuedeHacerDash)
        {
            StartCoroutine(Dash());
        }


        if (Input.GetKeyDown(KeyCode.Y))
        {
            velocidadDash = velocidadDashOriginal;
            CoolDownDash = coolDownDashOriginal;
            velocidadDeMovimiento = velocidadDeMovimientoOriginal;
            SavePlayerPrefs();
        }

    }

    private void SavePlayerPrefs()
    {

        PlayerPrefs.SetInt("dashSpeedUpgraded", 0);
        PlayerPrefs.SetInt("dashCoolDownUpgraded", 0);
        PlayerPrefs.SetInt("movementSpeedUpgraded", 0);
        PlayerPrefs.SetFloat("velocidadDash", velocidadDash);
        PlayerPrefs.SetFloat("velocidadDeMovimiento", velocidadDeMovimiento);
        PlayerPrefs.SetFloat("CoolDownDash", CoolDownDash);
        PlayerPrefs.Save();
    }

    private void FixedUpdate()
    {
        enSuelo = Physics2D.OverlapBox(controladorSuelo.position, dimensionesCaja, 0f, queEsSuelo);
        animator.SetBool("enSuelo",enSuelo);
        //Movimiento
        if(CanMove)
        {
            Mover(movimientoHorizontal * Time.fixedDeltaTime, salto);
        }

        salto = false;
    }

    private void Mover(float mover, bool saltar)
    {
        Vector3 velocidadObjetivo = new Vector2(mover, rb2D.velocity.y);// + platformSpeed;
        rb2D.velocity = velocidadObjetivo; // Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadodeMovimiento);

        if (mover > 0 && !mirandoDerecha)
        {
            //Gira
            Girar();
        }
        else if (mover < 0 && mirandoDerecha)
        {
            //Gira
            Girar();
        }

        if(enSuelo && saltar)
        {
            enSuelo = false;
            rb2D.AddForce(new Vector2(0f, fuerzaDeSalto));
        }
    }

    private IEnumerator Dash()
    {
        CanMove = false;
        PuedeHacerDash = false;
        Physics2D.IgnoreLayerCollision(6, 3, true);
        rb2D.gravityScale = 0;
        rb2D.velocity = new Vector2(velocidadDash * transform.localScale.x, 0);
        animator.SetTrigger("Dash");
        trailRenderer.emitting = true;


        yield return new WaitForSeconds(tiempoDash);

        rb2D.gravityScale = gravedadInicial;
        CanMove = true;
        yield return new WaitForSeconds(0.25f);
        Physics2D.IgnoreLayerCollision(6, 3, false);

        yield return new WaitForSeconds(CoolDownDash);
        PuedeHacerDash = true;
        trailRenderer.emitting = false;
        
        
    }

    private void Girar ()
    {
        mirandoDerecha = !mirandoDerecha;
        lookCheck = !lookCheck;
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(controladorSuelo.position, dimensionesCaja);
    }

    public void Rebote(Vector2 puntoGolpe)
    {
        rb2D.velocity = new Vector2((-velocidadRebote.x +15) * puntoGolpe.x, (velocidadRebote.y - 4));
    }
    public void UpgradeDashSpeed()
    {
        if (!dashSpeedUpgraded)
        {
            gameManager.puntosTotales -= 450;
            velocidadDash = 40f;
            PlayerPrefs.SetFloat("velocidadDash", velocidadDash);
            dashSpeedUpgraded = true;
            PlayerPrefs.SetInt("dashSpeedUpgraded", 1);
            Debug.Log("Dash speed upgraded!");
        }
    }

    public void UpgradeMovementSpeed()
    {
        if (!movementSpeedUpgraded)
        {
            gameManager.puntosTotales -= 450;
            velocidadDeMovimiento = 500f;
            PlayerPrefs.SetFloat("velocidadDeMovimiento", velocidadDeMovimiento);
            movementSpeedUpgraded = true;
            PlayerPrefs.SetInt("movementSpeedUpgraded", 1);
            Debug.Log("Movement speed upgraded!");
        }
    }

    public void UpgradeDashCooldown()
    {
        if (!dashCoolDownUpgraded)
        {
            gameManager.puntosTotales -= 450;
            CoolDownDash = 0.5f;
            PlayerPrefs.SetFloat("CoolDownDash", CoolDownDash);
            dashCoolDownUpgraded = true;
            PlayerPrefs.SetInt("dashCoolDownUpgraded", 1);
            Debug.Log("Dash cooldown reduced!");
        }
    }
    
}

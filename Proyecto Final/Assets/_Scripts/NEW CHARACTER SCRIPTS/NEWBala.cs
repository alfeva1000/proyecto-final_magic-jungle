using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWBala : MonoBehaviour
{
    [SerializeField] private float velocidad;
    [SerializeField] private float daño;

    public NEWControlJugador controller2; 
    private bool boolcheck;

   
    

    private void Start()
    {
        controller2 = FindObjectOfType<NEWControlJugador>();
        boolcheck = controller2.lookCheck;
        
        
    }

    private void Update()
    {
        if (boolcheck == false)
        {
            transform.Translate(Vector2.right * velocidad * Time.deltaTime);
        }   
        else
        {
            transform.Translate(Vector2.left * velocidad * Time.deltaTime);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemies"))
        {
            other.GetComponent<NEWEnemigo>().TomarDañoEnemigo(daño);
            Destroy(gameObject);
        }
        else if (other.CompareTag("Walls"))
        {
            Destroy(gameObject);
        }
    }
}

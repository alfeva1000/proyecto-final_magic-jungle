using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWPlayerCombat : MonoBehaviour
{
    [SerializeField] private float vida;
    private NEWControlJugador controlJugador;
    [SerializeField] private float tiempoPerdidaControl;
    private bool dmgTaken;
    public Sprite newSprite;
    private SpriteRenderer spriteRenderer;
    private bool canTakedmg = true;

    private Animator animator;

    private void Start()
    {
        controlJugador = GetComponent<NEWControlJugador>();
        animator = GetComponent<Animator>();
        animator.SetBool("PlayerDMG", dmgTaken);
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void TomarDaño1(float daño)
    {
        vida-= daño;
        dmgTaken = true;
        StartCoroutine(Blink(2f, 0.05f));
        StartCoroutine(PerderControl());
        //DesactivarColisión de Enemigos.
        StartCoroutine(DesactivarColisión());
        dmgTaken = false;
    }
    
    public void TomarDaño(float daño, Vector2 posicion)
    {
        vida-= daño;
        dmgTaken = true;
        StartCoroutine(Blink(2f, 0.05f));
        StartCoroutine(PerderControl());
        //DesactivarColisión de Enemigos.
        StartCoroutine(DesactivarColisión());
        controlJugador.Rebote(posicion);
        dmgTaken = false;
    }

    private IEnumerator DesactivarColisión()
    {
        Physics2D.IgnoreLayerCollision(6, 3, true);
        yield return new WaitForSeconds(2f);
        Physics2D.IgnoreLayerCollision(6, 3, false);
    }
    private IEnumerator Blink(float duration, float blinkInterval)
        {
            float endTime = Time.time + duration;
        
        while (Time.time < endTime)
            {
                spriteRenderer.sprite = newSprite;
                spriteRenderer.enabled = !spriteRenderer.enabled;
                yield return new WaitForSeconds(blinkInterval);
            }
            spriteRenderer.enabled = true;
        }
    private IEnumerator PerderControl()
    {
        
        controlJugador.CanMove = false;
        yield return new WaitForSeconds(tiempoPerdidaControl);
        controlJugador.CanMove = true;
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{

    public void Level1()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Jungle Level 1final");
    }

     public void Level2()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Jungle Level 2final");
    }

     public void Level3()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Castle Level 1final");
    }

    public void Level4()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Castle Level 2final");
    }

    public void LOBBY()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("LobbyFinal");
    }

    public void Exit()
    {
        Debug.Log("Salir...");
        Application.Quit();
    }
 
}

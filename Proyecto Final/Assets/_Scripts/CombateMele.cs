using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateMele : MonoBehaviour
{
    [Header("Melee")]
    [SerializeField] private Transform controladorGolpe;
    [SerializeField] private float radioGolpe;
    [SerializeField] private float dañoGolpe;
    [SerializeField] private float tiempoEntreAtaquesMelee;
    [SerializeField] private float tiempoSiguienteAtaqueMelee;
    public bool tiempoEntreAtaquesMeleeUpgraded = false;

    [Header("Range")]
    [SerializeField] private Transform controladorDisparo;
    [SerializeField] private GameObject bala;
    [SerializeField] private float tiempoEntreAtaquesRange;
    [SerializeField] private float tiempoSiguienteAtaqueRange;
    public bool tiempoEntreAtaquesRangeUpgraded = false;
    public SpriteRenderer sprite;
    public NEWControlJugador controller;
    private bool boolcheck;
    public GameManager gameManager;

    private Animator animator;


    private void Start()
    {
        controller = FindObjectOfType<NEWControlJugador>();
        animator = GetComponent<Animator>();
        tiempoEntreAtaquesRangeUpgraded = PlayerPrefs.GetInt("tiempoEntreAtaquesRangeUpgraded", 0) == 1;
        tiempoEntreAtaquesMeleeUpgraded = PlayerPrefs.GetInt("tiempoEntreAtaquesMeleeUpgraded", 0) == 1;

        tiempoEntreAtaquesMelee = PlayerPrefs.GetFloat("tiempoEntreAtaquesMelee", 1f);
        tiempoEntreAtaquesRange = PlayerPrefs.GetFloat("tiempoEntreAtaquesRange", 1.5f);
    }

    private void Update()
    {
        if (tiempoSiguienteAtaqueMelee > 0)
        {
            tiempoSiguienteAtaqueMelee -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1") && tiempoSiguienteAtaqueMelee <= 0)
        {
            Golpe();
            tiempoSiguienteAtaqueMelee = tiempoEntreAtaquesMelee;
        }


        if (tiempoSiguienteAtaqueRange > 0)
        {
            tiempoSiguienteAtaqueRange -= Time.deltaTime;
        }
        if (Input.GetButtonDown("Fire2") && tiempoSiguienteAtaqueRange <= 0)
        {
            Disparar();
            tiempoSiguienteAtaqueRange = tiempoEntreAtaquesRange;
        }

        if (Input.GetKeyDown(KeyCode.Y)) 
        {
            tiempoEntreAtaquesRangeUpgraded = false;
            tiempoEntreAtaquesMeleeUpgraded = false;

            
            PlayerPrefs.SetInt("tiempoEntreAtaquesRangeUpgraded", tiempoEntreAtaquesRangeUpgraded ? 1 : 0);
            PlayerPrefs.SetInt("tiempoEntreAtaquesMeleeUpgraded", tiempoEntreAtaquesMeleeUpgraded ? 1 : 0);

            PlayerPrefs.SetFloat("tiempoEntreAtaquesMelee", 1f);
            PlayerPrefs.SetFloat("tiempoEntreAtaquesRange", 1.5f);

        }


    }

    private void Golpe()
    {
        animator.SetTrigger("Golpe");

        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorGolpe.position, radioGolpe);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Enemies"))
            {
                colisionador.transform.GetComponent<NEWEnemigo>().TomarDañoEnemigo(dañoGolpe);
                
            }
            if (colisionador.CompareTag("Boss"))
            {
                var enemigoBoss = colisionador.transform.GetComponent<NEWEnemigoBosses>();
                if (enemigoBoss != null)
                {
                    enemigoBoss.TomarDañoEnemigo(dañoGolpe);
                }
            }
        }
    }

    private void Disparar()
    {
        animator.SetTrigger("Disparo");

        Instantiate(bala, controladorDisparo.position, controladorDisparo.rotation);
    }



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(controladorGolpe.position, radioGolpe);
    }
    public void UpgradeMeleeCooldown()
    {
        if (!tiempoEntreAtaquesMeleeUpgraded)
        {
            gameManager.puntosTotales -= 450;
            tiempoEntreAtaquesMelee = 0.5f;
            PlayerPrefs.SetFloat("tiempoEntreAtaquesMelee", tiempoEntreAtaquesMelee);
            tiempoEntreAtaquesMeleeUpgraded = true;
            PlayerPrefs.SetInt("tiempoEntreAtaquesMeleeUpgraded", 1);

        }
    }
    public void UpgradeRangedCooldown()
    {
        if (!tiempoEntreAtaquesRangeUpgraded)
        {
            gameManager.puntosTotales -= 450;
            tiempoEntreAtaquesRange = 0.75f;
            PlayerPrefs.SetFloat("tiempoEntreAtaquesRange", tiempoEntreAtaquesRange);
            tiempoEntreAtaquesRangeUpgraded = true;
            PlayerPrefs.SetInt("tiempoEntreAtaquesRangeUpgraded", 1);

        }
    }

}



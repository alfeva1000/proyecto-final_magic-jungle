using System.Collections.Generic;
using UnityEngine;

public class FondoMovimiento : MonoBehaviour
{
    [SerializeField] private Vector2 velocidadMovimiento;
    private Vector2 offset;
    private Material material;

    private Transform jugadorTransform;
    [SerializeField] List<Vector2> movimientoList;
    [SerializeField]  int samples = 5;
    [SerializeField]  int currentSample = 0;
    [SerializeField]  private Vector2 playerLocalSpeed;
    [SerializeField]  private Vector2 playerLastPosition;
    [SerializeField]
    private float ponderationOfIndividualSampleInTheAverage;

    [SerializeField] float multiplerSpeed=0.05f;

    private void Awake()
    {
        material = GetComponent<SpriteRenderer>().material;
        jugadorTransform = FindObjectOfType<NEWControlJugador>().transform;

        movimientoList = new List<Vector2>();
        for (int i = 0; i < samples; i++)
        {
            movimientoList.Add(Vector2.zero);
        }
        ponderationOfIndividualSampleInTheAverage = 1f / (float)samples;
    }

    private void Update()
    {
         int sampleIndex = currentSample % samples;
        Vector2 sampleToRemove = movimientoList[sampleIndex];
        movimientoList[sampleIndex] = jugadorTransform.position - (Vector3)playerLastPosition;
        playerLastPosition = jugadorTransform.position;

        playerLocalSpeed += (movimientoList[sampleIndex] * ponderationOfIndividualSampleInTheAverage)
            - (ponderationOfIndividualSampleInTheAverage * sampleToRemove);

        if (currentSample > 10000000)
        {
            currentSample = 0;
        }
        currentSample++;



        offset = (playerLocalSpeed.x *multiplerSpeed) * velocidadMovimiento * Time.deltaTime;
        material.mainTextureOffset += offset;
    }
}

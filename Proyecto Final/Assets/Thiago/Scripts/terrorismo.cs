using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class terrorismo : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject portalBoss;
    public GameObject portalBoss2;
    // Start is called before the first frame update
    void Start()
    {
        portalBoss.SetActive(false);
        portalBoss2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.parch >=2)
        {
            portalBoss.SetActive(true);
        }
        if (gameManager.parch >= 4)
        {
            portalBoss2.SetActive(true);
        }
    }
}

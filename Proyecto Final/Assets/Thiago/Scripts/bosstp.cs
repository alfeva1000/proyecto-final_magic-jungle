using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class bosstp : MonoBehaviour
{
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        gameManager = FindObjectOfType<GameManager>();
        if (other.gameObject.CompareTag("Player"))
        {
            gameManager.SaveCoins();
            gameManager.SaveParch();
            SceneManager.LoadScene("Boss1");
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

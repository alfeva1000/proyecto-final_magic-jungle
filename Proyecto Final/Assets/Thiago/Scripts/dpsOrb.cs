using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dpsOrb : MonoBehaviour
{
    public GameManager gameManager;
    public FinalBossController finalBossController;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Player"))
        {
            gameManager.orbPickup++;
            gameObject.SetActive(false);
        }
        

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

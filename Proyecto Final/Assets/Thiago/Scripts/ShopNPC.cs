using TarodevController;
using UnityEngine;
using UnityEngine.UI;

public class ShopTrigger2D : MonoBehaviour
{
    public GameObject shopPromptText; 
    public GameObject shopUI; 
    public Button upgradeDashSpeedButton; 
    public Button upgradeMovementSpeedButton; 
    public Button upgradeDashCooldownButton;
    public Button UpgradeMeleeCooldown;
    public Button UpgradeRangeCooldown;
    public GameManager GameManager;
    private bool playerInRange = false;
    public NEWControlJugador playerController;
    public CombateMele playerCombat;
    public GameObject menuPausa;
    


    private void Start()
    {
        

        if (PlayerPrefs.GetInt("dashSpeedUpgraded", 0) == 1)
        {
            upgradeDashSpeedButton.interactable = false;
        }

        if (PlayerPrefs.GetInt("movementSpeedUpgraded", 0) == 1)
        {
            upgradeMovementSpeedButton.interactable = false;
        }

        if (PlayerPrefs.GetInt("dashCoolDownUpgraded", 0) == 1)
        {
            upgradeDashCooldownButton.interactable = false;
        }

        if (PlayerPrefs.GetInt("tiempoEntreAtaquesRangeUpgraded", 0) == 1)
        {
            UpgradeRangeCooldown.interactable = false;
        }

        if (PlayerPrefs.GetInt("tiempoEntreAtaquesMeleeUpgraded", 0) == 1)
        {
            UpgradeMeleeCooldown.interactable = false;
        }

        if (PlayerPrefs.GetInt("dashCoolDownUpgraded", 0) == 0)
        {
            upgradeDashCooldownButton.interactable = true;
        }

        if (PlayerPrefs.GetInt("tiempoEntreAtaquesRangeUpgraded", 0) == 0)
        {
            UpgradeRangeCooldown.interactable = true;
        }

        if (PlayerPrefs.GetInt("tiempoEntreAtaquesMeleeUpgraded", 0) == 0)
        {
            UpgradeMeleeCooldown.interactable = true;
        }


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            shopPromptText.gameObject.SetActive(true);
            playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            shopPromptText.gameObject.SetActive(false);
            playerInRange = false;
        }
    }

    private void Update()
    {
        if (playerInRange && Input.GetKeyDown(KeyCode.O))
        {
            OpenShop();
        }
    }

    private void OpenShop()
    {
        shopUI.SetActive(true);
        Time.timeScale = 0f;
        shopPromptText.gameObject.SetActive(false); 
        playerController.CanMove = false; 
        Cursor.visible = true; 
        Cursor.lockState = CursorLockMode.None; 
    }

    public void CloseShop()
    {
        Time.timeScale = 1f;
        shopUI.SetActive(false); 
        playerController.CanMove = true; 
        Cursor.visible = true; 
        Cursor.lockState = CursorLockMode.None; 
    }

    
    public void OnUpgradeDashSpeed()
    {
        if (GameManager.PuntosTotales >= 450 && upgradeDashSpeedButton.interactable)
        {
            playerController.UpgradeDashSpeed();
            upgradeDashSpeedButton.interactable = false;
        }
            
    }

    public void OnUpgradeMovementSpeed()
    {
        if (GameManager.PuntosTotales >= 450 && upgradeMovementSpeedButton.interactable)
        {
            playerController.UpgradeMovementSpeed();
            upgradeMovementSpeedButton.interactable = false;
        }
        
    }

    public void OnUpgradeDashCooldown()
    {
        if (GameManager.PuntosTotales >= 450 && upgradeDashCooldownButton.interactable)
        {
            playerController.UpgradeDashCooldown();
            upgradeDashCooldownButton.interactable = false;
        }
        
    }
    public void OnUpgradeAttackSpeed()
    {
        if (GameManager.PuntosTotales >= 450 && UpgradeMeleeCooldown.interactable)
        {
            playerCombat.UpgradeMeleeCooldown();
            UpgradeMeleeCooldown.interactable = false;
        }

    }
    public void OnUpgradeRangeSpeed()
    {
        if (GameManager.PuntosTotales >= 450 && UpgradeRangeCooldown.interactable)
        {
            playerCombat.UpgradeRangedCooldown();
            UpgradeRangeCooldown.interactable = false;
        }

    }
}

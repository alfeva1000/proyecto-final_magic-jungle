







using System.Collections;
using System.Collections.Generic;


using UnityEngine;

public class FinalBossController : MonoBehaviour
{
    private bool lockAnimation = false;
    public SpriteRenderer spriteRenderer;
    public int atknumber = 0;
    public bool fightstarted = false;
    [SerializeField] private GameObject efecto;
    public Animator animator;
    public Vector3 teleportPosition;
    public Transform destinationTransform;
    private bool isAttacking;
    public BossManager bossManager;
    private bool inDamagePhase = false;
    private Coroutine randomAttackCoroutine;


    public Transform attack3Transform;
    public Transform dpsTransform;
    [SerializeField] private float vidaEnemigo;
    private bool isResting;
    public bool dpsAvailable;
    public GameObject magusDoor;
    public bool playerEntered = false;

    public GameObject spawnhongo;
    public GameObject spawnhongoluz;
    public GameObject spawnhongodark;
    public GameObject slime;
    public Transform attack1Transform;
    public Transform attack1Transform2;
    private GameObject[] spawnObjects;
    private Transform[] spawnTransforms;

    public GameObject saw;
    public Transform pointA; 
    public Transform pointB; 
    public float[] speeds;
    private float currentSpeed; 
    private bool movingTowardsB = true;
    private bool touchdone;
    private int touches;
    private bool coro1;

    public GameObject lava;
    public Transform lavaStartTransform;
    public Transform lavaEndTransform;
    public float lavaMoveDuration = 20.0f;
    public GameObject dpsOrb1;
    public GameObject dpsOrb2;
    public GameObject dpsOrb3;
    public GameObject dpsOrb4;
    public Transform position1Transform;
    public GameObject parkour1;
    public Transform position2Transform;
    public GameObject parkour2;
    public Transform originalTransform;


    void Start()
    {
        parkour1.SetActive(false);
        parkour2.SetActive(false);
        saw.SetActive(false);
        bossManager = FindObjectOfType<BossManager>();
        coro1 = false;
        spawnObjects = new GameObject[] { spawnhongo, spawnhongoluz, spawnhongodark, slime };
        spawnTransforms = new Transform[] { attack1Transform, attack1Transform2 };
    }

    // Update is called once per frame
    void Update()
    {
        if (atknumber >= 1)
        {
            StopCoroutine(CallChooseRandomAttackRepeatedly());
        }
    }
    public void Attack()
    {
        
        
        StartCoroutine(Buffer());
    }
    public void Dmg()
    {
        
        StartCoroutine(DamagePhases());
    }
    IEnumerator Buffer()
    {
        yield return new WaitForSeconds(5f);
        StartCoroutine(CallChooseRandomAttackRepeatedly());

    }
    IEnumerator CallChooseRandomAttackRepeatedly()
    {
        yield return new WaitForSeconds(0.4f);
        ChooseRandomAttack();

    }
    public void ChooseRandomAttack()
    {

        Debug.Log("hola");
        if (inDamagePhase)
        {
            
            return;
        }
        else 
        {
            if (atknumber >= 1)
            {
                
                StartCoroutine(DpsPhase());
                Debug.Log("dps");
            }
            else
            {
                parkour2.SetActive(false);
                parkour1.SetActive(false);
                int randomAttack = Random.Range(1, 3);

                switch (randomAttack)
                {
                    case 1:
                        StartCoroutine(Attack1Coroutine());
                        break;
                    case 2:
                        if (!coro1)
                        {
                            StartCoroutine(Attack2Coroutine());
                        }
                        if (coro1)
                        {
                            StartCoroutine(Attack1Coroutine());
                        }

                        break;
                    default:
                        Debug.LogError("Invalid attack choice!");
                        break;
                }
            }
        }
        
    }
    void StopRandomAttackCoroutine()
    {
        if (randomAttackCoroutine != null)
        {
            StopCoroutine(randomAttackCoroutine);
            randomAttackCoroutine = null;
        }
    }
    IEnumerator Attack1Coroutine()
    {
        Debug.Log("hola2");
        atknumber++;
        isAttacking = true;
        
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < 2; i++)
        {
           
            GameObject objectToSpawn = spawnObjects[Random.Range(0, spawnObjects.Length)];
            Transform spawnTransform = spawnTransforms[Random.Range(0, spawnTransforms.Length)];

            
            Instantiate(objectToSpawn, spawnTransform.position, spawnTransform.rotation);

            yield return new WaitForSeconds(1.7f);
        }
        yield return new WaitForSeconds(7f);
        ChooseRandomAttack();
        isAttacking = false;
        
    }
    IEnumerator Attack2Coroutine()
    {
        atknumber++;
        
        saw.SetActive(true);
        touchdone = false;
        touches = 0;

        yield return new WaitForSeconds(0.6f);
        float elapsedTime = 0f; // Tracks time since object started moving
        while (elapsedTime < 10f) // Run for 10 seconds
        {
            coro1 = true;
            float currentSpeed = speeds[Random.Range(0, speeds.Length)];
            Vector3 targetPosition = movingTowardsB ? pointB.position : pointA.position;
            Vector3 startPosition = saw.transform.position;
            float journeyLength = Vector3.Distance(startPosition, targetPosition);
            float startTime = Time.time;

                // Check if the saw needs to flip its sprite
                if (movingTowardsB && saw.transform.localScale.x > 0)
                {
                    // Flip the sprite if moving towards point B and it's not already flipped
                    saw.transform.localScale = new Vector3(-saw.transform.localScale.x, saw.transform.localScale.y, saw.transform.localScale.z);
                }
                else if (!movingTowardsB && saw.transform.localScale.x < 0)
                {
                    // Flip the sprite if moving towards point A and it's not already flipped
                    saw.transform.localScale = new Vector3(-saw.transform.localScale.x, saw.transform.localScale.y, saw.transform.localScale.z);
                }

                while (saw.transform.position != targetPosition)
                {
                    float distCovered = (Time.time - startTime) * currentSpeed;
                    float fractionOfJourney = distCovered / journeyLength;
                    saw.transform.position = Vector3.Lerp(startPosition, targetPosition, fractionOfJourney);
                    elapsedTime += Time.deltaTime;
                    yield return null;
                }

                // Switch direction
                movingTowardsB = !movingTowardsB;
        }
            saw.SetActive(false);
            coro1 = false;
        ChooseRandomAttack();
    }


    private IEnumerator DpsPhase()
    {
        StopRandomAttackCoroutine();
        StopCoroutine(CallChooseRandomAttackRepeatedly());
        yield return new WaitForSeconds(0.5f);

        spriteRenderer.enabled = false;

        yield return new WaitForSeconds(0.5f);


        int platformchoose = Random.Range(1, 3);

        switch (platformchoose)
        {
            case 1:
                transform.position = position1Transform.position;
                yield return new WaitForSeconds(0.5f);
                spriteRenderer.enabled = true;
                parkour1.SetActive(true);
                dpsOrb1.SetActive(true);
                dpsOrb2.SetActive(true);
                lava.SetActive(true);
                StartCoroutine(MoveLava());
                break;
            case 2:
                transform.position = position1Transform.position;
                yield return new WaitForSeconds(0.5f);
                spriteRenderer.enabled = true;
                parkour2.SetActive(true);
                dpsOrb3.SetActive(true);
                dpsOrb4.SetActive(true);
                lava.SetActive(true);
                StartCoroutine(MoveLava());
                break;
            default:
                Debug.LogError("Invalid attack choice!");
                break;
        }
    }
    public IEnumerator DamagePhases()
    {
        inDamagePhase = true;
        StopCoroutine(CallChooseRandomAttackRepeatedly());
        StopRandomAttackCoroutine();
        lava.SetActive(false);
        
        yield return new WaitForSeconds(20f);
        inDamagePhase = false;
        
        yield return new WaitForSeconds(0.3f);

        spriteRenderer.enabled = false;

        yield return new WaitForSeconds(0.3f);
        transform.position = originalTransform.position;
        parkour2.SetActive(false);
        parkour1.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = true;
        atknumber = 0;
        randomAttackCoroutine = StartCoroutine(CallChooseRandomAttackRepeatedly());

    }

    private IEnumerator MoveLava()
    {
        float elapsedTime = 0f;
        Vector2 startingPos = lavaStartTransform.position;
        Vector2 endingPos = lavaEndTransform.position;

        while (elapsedTime < lavaMoveDuration)
        {
            lava.transform.position = Vector2.Lerp(startingPos, endingPos, elapsedTime / lavaMoveDuration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // Ensure the lava reaches the final position
        lava.transform.position = endingPos;
        
        

    }
    
    

}

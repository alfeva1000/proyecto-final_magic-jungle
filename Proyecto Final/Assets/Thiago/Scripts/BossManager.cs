
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public BossController bossController;
    public GameObject magusDoor;
    public FinalBossController finalBossController;

    public GameObject saw;
    public Transform pointA;
    public Transform pointB;
    public float[] speeds;
    private float currentSpeed;
    private bool movingTowardsB = true;
    private bool touchdone;
    private int touches;


    void Start()
    {
        saw.SetActive(false);
        bossController = FindObjectOfType<BossController>();
        finalBossController = FindObjectOfType<FinalBossController>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("Player") && bossController != null)
        {
            magusDoor.SetActive(true);
            bossController.TriggerBossAttack();
        }
        if (other.CompareTag("Player") && finalBossController != null)
        {
            magusDoor.SetActive(true);
            finalBossController.Attack();
        }

    }
    


    void Update()
    {
        
    }
}

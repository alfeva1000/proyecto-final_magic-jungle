

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    
    private bool lockAnimation = false;
    public SpriteRenderer spriteRenderer;
    public int atknumber = 0;
    public bool fightstarted = false;
    [SerializeField] private GameObject efecto;
    public Animator animator;
    public Vector3 teleportPosition;
    public Transform destinationTransform;
    public Transform attack1Transform;
    public Transform attack2Transform;
    public Transform attack3Transform;
    public Transform dpsTransform;
    [SerializeField] private float vidaEnemigo;
    private bool isResting;
    public bool dpsAvailable;
    public GameObject magusDoor;
    public bool playerEntered = false;

    public GameObject swipesinderechos;
    public GameObject swipeconderechos;
    public float spawnDelay = 0.6f; 
    public int numberOfSpawns = 5;
    private GameObject spawnedObject;
    [SerializeField] private float fixedYPosition;
    [SerializeField] private float spawnXPosition;
    public float moveSpeed = 5f;
    public float projectileDuration = 2.2f;

    public Transform[] spawnPositions; 
    public Transform[] targetPositions;
    private GameObject[] spawnedObjects;
    public float spawnDelay2 = 0.5f; 
    public float moveDuration2 = 1.0f;
    public GameObject objectPrefab;

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void TriggerBossAttack()
    {
        
        Debug.Log("Triggering boss attack");
        StartCoroutine(WaitForBossAttack());
    }

    IEnumerator WaitForBossAttack()
    {
        yield return new WaitForSeconds(2f);
        if (!isResting)
        {
            ChooseRandomAttack();
        }
    }

    void ChooseRandomAttack()
    {
        fightstarted = true;

        if (atknumber >= 5)
        {
            StartCoroutine(DpsPhase());
            Debug.Log("dps");
        }
        else
        {
            int randomAttack = Random.Range(1, 3);

            switch (randomAttack)
            {
                case 1:
                    StartCoroutine(Attack1Coroutine());
                    break;
                case 2:
                    StartCoroutine(Attack2Coroutine());
                    break;
                default:
                    Debug.LogError("Invalid attack choice!");
                    break;
            }
        }
    }

    IEnumerator Attack1Coroutine()
    {
        atknumber++;
        float projectileDuration = 2.2f;
        float elapsedTime = 0f;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.3f);
        /*if (swipe != null) esto es para el audio
        {
            swipe.Play();
        }
        */ 
        transform.position = attack1Transform.position;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = true;
        animator.SetBool("Swipe", true);
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < numberOfSpawns; i++)
        {
            lockAnimation = true;
            if (lockAnimation && animator != null)
            {
                animator.Play(animator.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, 1f);
                animator.speed = 0f;
            }
            GameObject objToSpawn = (Random.value > 0.5f) ? swipesinderechos : swipeconderechos;
            GameObject spawnedObject = Instantiate(objToSpawn, new Vector3(spawnXPosition, fixedYPosition, 0f), Quaternion.identity);          
            Vector3 direction = Vector3.right;
            Rigidbody2D rb = spawnedObject.GetComponent<Rigidbody2D>();
            rb.velocity = direction * moveSpeed;      
            StartCoroutine(ManageProjectileLifetime(spawnedObject));
            yield return new WaitForSeconds(spawnDelay);
        }
        animator.SetBool("Swipe", false);
        lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }
        Destroy(spawnedObject); 
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(Teleport());
    }

    IEnumerator Attack2Coroutine()
    {
        spawnedObjects = new GameObject[spawnPositions.Length];

        atknumber++;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.3f);
        /*if (swipe != null) esto es para el audio
        {
            swipe.Play();
        }
        */
        transform.position = attack2Transform.position;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = true;
        spriteRenderer.flipX = true;
        animator.SetBool("Snap", true);
        yield return new WaitForSeconds(0.7f);
        Quaternion spawnRotation = Quaternion.Euler(0, 0, -90);
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            GameObject spawnedObject = Instantiate(objectPrefab, spawnPositions[i].position, spawnRotation);
            spawnedObjects[i] = spawnedObject;
            StartCoroutine(MoveObject(spawnedObject, targetPositions[i].position, moveDuration2, spawnRotation));
            yield return new WaitForSeconds(spawnDelay2);
        }
        yield return new WaitForSeconds(moveDuration2);
        Quaternion returnRotation = Quaternion.Euler(0, 0, 90);
        for (int i = 0; i < spawnedObjects.Length; i++)
        {
            StartCoroutine(MoveObject(spawnedObjects[i], spawnPositions[i].position, moveDuration2, returnRotation));
        }
        yield return new WaitForSeconds(moveDuration2);
        yield return new WaitForSeconds(spawnDelay2);
        for (int i = 0; i < spawnedObjects.Length; i++)
        {
            StartCoroutine(MoveObject(spawnedObjects[i], targetPositions[i].position, moveDuration2, spawnRotation));
        }
        yield return new WaitForSeconds(moveDuration2);
        for (int i = 0; i < spawnedObjects.Length; i++)
        {
            Destroy(spawnedObjects[i]);
        }
        yield return new WaitForSeconds(0.3f);
        animator.SetBool("Snap", false);
        StartCoroutine(Teleport());
    }
    private IEnumerator MoveObject(GameObject obj, Vector3 targetPosition, float duration, Quaternion targetRotation)
    {
        Vector3 startPosition = obj.transform.position;
        Quaternion startRotation = obj.transform.rotation;
        float elapsedTime = 0;
        float rotationSpeedMultiplier = 300.0f;
        while (elapsedTime < duration)
        {
            if (obj == null) yield break;
            obj.transform.position = Vector3.Lerp(startPosition, new Vector3(startPosition.x, targetPosition.y, startPosition.z), elapsedTime / duration);
            obj.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, elapsedTime / (duration / rotationSpeedMultiplier));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        if (obj != null)
        {
            obj.transform.position = new Vector3(startPosition.x, targetPosition.y, startPosition.z);
            obj.transform.rotation = targetRotation;
        }
    }

    private IEnumerator MoveObjectAndDespawn(GameObject obj, Vector3 targetPosition, float duration, Quaternion endRotation)
    {
        Vector3 startPosition = obj.transform.position;
        float elapsedTime = 0;

        while (elapsedTime < duration)
        {
            if (obj == null) yield break; 
            obj.transform.position = Vector3.Lerp(startPosition, new Vector3(startPosition.x, targetPosition.y, startPosition.z), elapsedTime / duration);
            obj.transform.rotation = Quaternion.Lerp(obj.transform.rotation, endRotation, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (obj != null)
        {
            obj.transform.position = new Vector3(startPosition.x, targetPosition.y, startPosition.z); 
            obj.transform.rotation = endRotation; 
            Destroy(obj); 
        }
    }


    private IEnumerator ManageProjectileLifetime(GameObject spawnedObject)
    {
        float elapsedTime = 0f;
        while (elapsedTime < projectileDuration)
        {
            elapsedTime += Time.deltaTime;

            if (spawnedObject == null)
                yield break;

            yield return null;
        }
    }
    private IEnumerator DpsPhase()
    {


        yield return new WaitForSeconds(0.5f);

        spriteRenderer.enabled = false;

        yield return new WaitForSeconds(0.5f);

        transform.position = dpsTransform.position;

        yield return new WaitForSeconds(0.5f);

        spriteRenderer.enabled = true;

        yield return new WaitForSeconds(1f);

        dpsAvailable = true;

        yield return new WaitForSeconds(1.5f);

        dpsAvailable = false;


        atknumber = 0;

        StartCoroutine(Teleport());

    }
    private IEnumerator Teleport()
    {
        spriteRenderer.flipX = false;
        //lockAnimation = false;
        if (animator != null)
        {
            animator.speed = 1f;
        }

        //shotCounter = 0;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(0.3f);
        transform.position = destinationTransform.position;
        yield return new WaitForSeconds(0.3f);
        spriteRenderer.flipX = false;
        spriteRenderer.enabled = true;
        yield return new WaitForSeconds(0.4f);
        ChooseRandomAttack();
    }


    public void TomarDañoEnemigo(float dañoEnemigo)
    {
        animator.SetTrigger("Golpe");
        vidaEnemigo -= dañoEnemigo;

        if (vidaEnemigo <= 0)
        {
            Muerte();
        }
    }
    private void Muerte()
    {
        Instantiate(efecto, transform.position, transform.rotation);
        transform.position = teleportPosition;
        Debug.Log("Teleported to: " + teleportPosition);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !playerEntered)
        {

            playerEntered = true;
            magusDoor.SetActive(true);
            TriggerBossAttack();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWEnemigoBosses : MonoBehaviour
{
    [SerializeField] private GameObject efecto;
    
    public Vector3 teleportPosition;
    public GameObject boss;
    public GameObject levelend;
    public Sprite blinkSprite; 
    private SpriteRenderer spriteRenderer;
    private Sprite originalSprite;
    private Animator animator;

    //public BossController bossController;
    // public string enemigos = "Enemies"


    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalSprite = spriteRenderer.sprite;
        animator = GetComponent<Animator>();
        // GameObject[] enemies = GameObject.FindObjectWithTag(enemigos);
        // foreach (GameObject enemies)
    }
    //Primera parte que va con las vidas del personaje
    

    //Segunda parte que va con las vidas de los enemigos

    [SerializeField] private float vidaEnemigo;

    public void TomarDañoEnemigo(float dañoEnemigo)
    {
        //animator.SetTrigger("Golpe");
        vidaEnemigo -= dañoEnemigo;
        StartCoroutine(Blink());
        if (vidaEnemigo <= 0)
        {
            Muerte();
        }
    }
    private IEnumerator Blink()
    {
        

        int blinkCount = 5;
        
        float blinkDuration = 0.1f;

        for (int i = 0; i < blinkCount; i++)
        {

            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(blinkDuration);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(blinkDuration);
        }
    }


    private void Muerte()
    {
        /*Instantiate(efecto, transform.position, transform.rotation);
        transform.position = teleportPosition;
        Debug.Log("Teleported to: " + teleportPosition);
        */
        Destroy(boss);
        levelend.SetActive(true);
        GameObject[] projectiles = GameObject.FindGameObjectsWithTag("Projectile");
        foreach (GameObject projectile in projectiles)
        {
            Destroy(projectile);
        }


    }

}

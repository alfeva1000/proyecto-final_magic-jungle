using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour
{

    public GameObject levelPromptText;
    public GameObject levelUI;
    public GameManager GameManager;
    public NEWControlJugador playerController;
    private bool playerInRange = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange && Input.GetKeyDown(KeyCode.O))
        {
            OpenLevelSelect();
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            levelPromptText.gameObject.SetActive(true);
            playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other == null || levelPromptText == null)
        {
            return;
        }

        if (other.CompareTag("Player"))
        {
            levelPromptText.gameObject.SetActive(false);
            playerInRange = false;
        }
    }
    private void OpenLevelSelect()
    {
        levelUI.SetActive(true);
        Time.timeScale = 0f;
        levelPromptText.gameObject.SetActive(false);
        playerController.CanMove = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void CloseLevelSelect()
    {
        Time.timeScale = 1f;
        levelUI.SetActive(false);
        playerController.CanMove = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
}

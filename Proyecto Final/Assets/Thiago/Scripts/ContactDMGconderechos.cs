using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ContactDMG1 : MonoBehaviour
{

    public GameObject player;
    public NEWPlayerCombat combat;
    public NEWControlJugador controlJugador;
    public NEWCambiarColor color;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Player") && color.Change == true)
        {
            other.gameObject.GetComponent<NEWPlayerCombat>().TomarDa�o1(1);
            GameManager.Instance.PerderVida();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
